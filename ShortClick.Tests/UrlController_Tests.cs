using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using ShortClick.Controllers;
using ShortClick.Services;
using System;
using System.Threading.Tasks;
using Xunit;

namespace ShortClick.Tests
{
    public class UrlController_Tests
    {
        private readonly Mock<ILogger<UrlController>> _logger;
        private readonly Mock<IUrlShortenerService> _shortenerService;
        private readonly Mock<ICacheDataService> _cache;

        private readonly UrlController _urlController;


        public UrlController_Tests()
        {
            _logger = new Mock<ILogger<UrlController>>();
            _shortenerService = new Mock<IUrlShortenerService>();
            _cache = new Mock<ICacheDataService>();
            _urlController = new UrlController(_logger.Object, _shortenerService.Object, _cache.Object);
        }

        [Fact]
        public async Task Create_ReturnsActionResultStringAsync()
        {
            var result = await _urlController.Create("");
            Assert.IsType<ActionResult<string>>(result);
        }

        [Fact]
        public async Task Open_ReturnsRedirectResultAsync()
        {
            var url = "some_url";
            var result = await _urlController.Open(url);
            Assert.IsType<RedirectResult>(result);
        }

        [Fact]
        public async Task Open_SavedTooManyValues()
        {
            // here should be some checks on more than 1000 values
            // but since used idistributed cache should be some different i guess
        }

        [Fact]
        public async Task Create_EmptyUrl()
        {
            var url = string.Empty;
            await Assert.ThrowsAsync<ArgumentNullException>(() => _urlController.Create(url));
        }
    }
}
