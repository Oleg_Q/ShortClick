﻿using System.Threading.Tasks;

namespace ShortClick.Services
{
    public interface ICacheDataService
    {
        Task<string> GetDataAsync(string key);

        Task SetDataAsync(string key, string value);
    }
}