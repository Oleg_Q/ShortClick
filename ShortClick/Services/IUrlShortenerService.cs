﻿using Microsoft.AspNetCore.Http;

namespace ShortClick.Services
{
    public interface IUrlShortenerService
    {
        string GetRandomShortUrl();

        string GetLink(string scheme, HostString hostString, string shortenUrl);
    }
}
