﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Threading.Tasks;

namespace ShortClick.Services.Implementation
{
    public class UrlCacheDataService : ICacheDataService
    {
        private readonly IDistributedCache _distributedCache;

        public UrlCacheDataService(IDistributedCache distributedCache)
        {
            _distributedCache = distributedCache;
        }

        public async Task<string> GetDataAsync(string key)
        {
            return await _distributedCache.GetStringAsync(key);
        }

        public async Task SetDataAsync(string key, string value)
        {
            var cacheOptions = new DistributedCacheEntryOptions()
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1),
                SlidingExpiration = TimeSpan.FromDays(7)
            };

            await _distributedCache.SetStringAsync(key, value, cacheOptions);
        }
    }
}
