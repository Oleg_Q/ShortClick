﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using System;

namespace ShortClick.Services.Implementation
{
    public class UrlShortenerService : IUrlShortenerService
    {
        public string GetRandomShortUrl()
        {
            return Guid.NewGuid().ToString().Substring(0, 5);
        }

        public string GetLink(string scheme, HostString hostString, string shortenUrl)
        {
            return UriHelper.BuildAbsolute(scheme, hostString) + shortenUrl;
        }
    }
}