using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ShortClick.Infrastructure;
using ShortClick.Services;
using ShortClick.Services.Implementation;
using System;
using System.Threading.Tasks;

namespace ShortClick
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen();

            services.AddDistributedSqlServerCache(options =>
            {
                options.ConnectionString = Configuration.GetConnectionString("DefaultConnection");
                options.SchemaName = "dbo";
                options.TableName = "Urls";
            });

            services.AddHttpContextAccessor();
            services.AddScoped<IUrlShortenerService, UrlShortenerService>();
            services.AddScoped<ICacheDataService, UrlCacheDataService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(opt =>
            {
                opt.RoutePrefix = string.Empty;
                opt.SwaggerEndpoint("/swagger/v1/swagger.json", "BadBroker API v1");
                opt.EnableTryItOutByDefault();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                // the same solution but +- more elegant. too late to do this i wanna sleep already
                //endpoints.MapGet("short_url", RedirectHelper.GetLink);
                //endpoints.MapFallback(RedirectHelper.RedirectToLinkRequestDelegate);
            });
        }

        
    }
}
