﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.Extensions.Logging;
using ShortClick.Services;
using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using System.Web;

namespace ShortClick.Controllers
{
    [ApiController]
    [Route("/")]
    public class UrlController : ControllerBase
    {
        private readonly ILogger<UrlController> _logger;
        private readonly IUrlShortenerService _urlShortenerService;
        private readonly ICacheDataService _cacheDataService;

        public UrlController(ILogger<UrlController> logger, 
            IUrlShortenerService urlShortenerService,
            ICacheDataService cacheDataService)
        {
            _logger = logger;
            _urlShortenerService = urlShortenerService;
            _cacheDataService = cacheDataService;
        }

        [HttpGet("short_url")]
        public async Task<ActionResult<string>> Create([Required] string url)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(url))
                    throw new ArgumentNullException();

                url = HttpUtility.HtmlEncode(url); // better would be some middleware xss check with special lib
                var shortUrl = _urlShortenerService.GetRandomShortUrl();
                await _cacheDataService.SetDataAsync(shortUrl, url);
                
                var link = _urlShortenerService.GetLink(Request.Scheme, Request.Host, shortUrl);
                return Ok(link);
            }
            catch (ArgumentNullException e)
            {
                _logger.LogError(e.Message);
                throw e;
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw e;
            }
        }

        [HttpGet("{shortUrl}")]
        public async Task<RedirectResult> Open(string shortUrl)
        {
            try
            {
                var url = await _cacheDataService.GetDataAsync(shortUrl);
                return Redirect(url ?? "/");
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                throw e;
            }
        }
    }
}
