﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using ShortClick.Services;
using System.Threading.Tasks;
using System.Web;

namespace ShortClick.Infrastructure
{
    public static class RedirectHelper
    {
        public static async Task RedirectToLinkRequestDelegate(HttpContext httpContext)
        {
            var cache = httpContext.RequestServices.GetRequiredService<ICacheDataService>();

            var path = httpContext.Request.Path.ToUriComponent().Trim('/').ToString();
            var url = await cache.GetDataAsync(path);

            httpContext.Response.Redirect(url ?? "/");

            await Task.CompletedTask;
        }

        public static async Task GetLink(HttpContext httpContext)
        {
            var cache = httpContext.RequestServices.GetRequiredService<ICacheDataService>();
            var urlShortener = httpContext.RequestServices.GetRequiredService<IUrlShortenerService>();

            var url = httpContext.Request.Query["url"];
            url = HttpUtility.HtmlEncode(url);


            var shortUrl = urlShortener.GetRandomShortUrl();
            var link = urlShortener.GetLink(httpContext.Request.Scheme, httpContext.Request.Host, shortUrl);

            await cache.SetDataAsync(shortUrl, url);
            await httpContext.Response.WriteAsync(link);
        }
    }
}
